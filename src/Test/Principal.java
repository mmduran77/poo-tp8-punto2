package Test;

import javax.swing.JOptionPane;

/**
 *
 * @author Mauro
 */
public class Principal {

    private static int nerd;
    private static double porcentaje;
    private static FormularioNerd f1 = new FormularioNerd();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //FormularioNerd f1 = new FormularioNerd();
        Preguntas  p = new Preguntas();
        //double porcentaje;
        int puntaje;
        JOptionPane.showMessageDialog(null, "Este test es para averiguar que tan NERD eres","TEST",1);
        puntaje=p.pregunta1();
        puntaje+=p.pregunta2();
        puntaje+=p.pregunta3();
        puntaje+=p.pregunta4();
        puntaje+=p.pregunta5();
        puntaje+=p.pregunta6();
        puntaje+=p.pregunta7();
        puntaje+=p.pregunta8();
        puntaje+=p.pregunta9();
        puntaje+=p.pregunta10();
        porcentaje = puntaje*100/110;
        calcularNerd(porcentaje);
        mostrarGradoNerd();
        
        
    }

    private static void calcularNerd(double porcentaje) {
        if (porcentaje>85){
            nerd=1;
        }else if(porcentaje>65){
            nerd=2;
        }else if(porcentaje>50){
            nerd=3;
        }else if(porcentaje>35){
            nerd=4;
        }else{
            nerd=5;
        }
            
    }

    private static void mostrarGradoNerd() {
        switch (nerd){
            case 1: FormularioNerd.txtPorcentaje.setText("RESULTADO: INCREIBLE ERES "+porcentaje+"% NERD");
                    FormularioNerd.txtDescripcion.setText("Eres súper nerd. ¡El nerdómetro está a punto de explotar contigo \n"
                            +"Desde chiquito/a fuiste una persona a la que le interesaba \n"
                            +"aprender y aun a tu edad no hay nada que te emocione más que el conocimiento.\n"
                            +"Tu curiosidad es imparable; siempre quieres saber de qué están \n"
                            +"hechas las cosas y aprender a hacerlas con tus propias manos. \n"
                            +"Pocas personas entienden tu mundo, pero las que sí lo hacen,\n"
                            +"son tus amistades más preciadas.");
                    f1.setVisible(true);
                    break;
            case 2: FormularioNerd.txtPorcentaje.setText("RESULTADO: ASOMBROSO ERES "+porcentaje+"% NERD");
                    FormularioNerd.txtDescripcion.setText("Tu porcentaje de nerd es alto, peeeero no llegas al máximo puntaje.\n"
                            +"Esto es porque, por un lado, te gusta la ciencia ficción, las matemáticas y \n"
                            +"los programas de tele científicos. Pero, por otro lado, no te la vives estudiando o \n"
                            +"pegado/a a la compu. Eres una persona a la que le interesa mucho pasar tiempo \n"
                            +"dentro de su mente, pero que también valora salir a echar desmadre con los amigos, \n"
                            +"reírse por cosas tontas y ver películas ligeras.");
                    f1.setVisible(true);
                    break; 
            case 3: FormularioNerd.txtPorcentaje.setText("RESULTADO: ASOMBROSO ERES "+porcentaje+"% NERD");
                    FormularioNerd.txtDescripcion.setText("En tu interior hay una lucha terrible entre dos lobos: \n"
                            +"Uno es malvado y está lleno de ira,\n"
                            +"el otro es un tetazo y escribe fan fiction de Dr. Who.\n"
                            +"Ganará aquel al que tú alimentes. La posta, el lobo nerd es más chido.");
                    f1.setVisible(true);
                    break;         
            case 4: FormularioNerd.txtPorcentaje.setText("RESULTADO: VAYA!! ERES "+porcentaje+"% NERD");
                    FormularioNerd.txtDescripcion.setText("Todo parece indicar que eres un nerdicurioso. \n"
                            +"No te identificas como un nerd pero compartes algunos de sus intereses. \n"
                            +"Que no te importe lo que diga la sociedad. Tú vive tu nerdés libre de prejuicios.");
                    f1.setVisible(true);
                    break;
            case 5: FormularioNerd.txtPorcentaje.setText("RESULTADO: NO ME SORPRENDE ERES "+porcentaje+"% NERD");
                    FormularioNerd.txtDescripcion.setText("No eres nada nerd. Es probable que no te gusten las matemáticas,\n"
                            +"ni los canales de ciencia, ni que te regalen libros. \n"
                            +"De hecho, hiciste este quiz solo para demostrar lo muy NO NERD que eres. \n"
                            +"Pero, ¿a poco no te dieron ganas de pasarte al lado nerd de la vida? \n"
                            +"¡Anímate, hay muchos cómics, juegos en línea y pelis de ciencia ficción!.");
                    f1.setVisible(true);
                    break;                
        }
    }

    
}
